#-------------------------------------------------
#
# Project created by QtCreator 2014-05-27T18:48:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Birds
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    bird.cpp \
    filter.cpp

HEADERS  += mainwindow.h \
    bird.h

FORMS    += mainwindow.ui
